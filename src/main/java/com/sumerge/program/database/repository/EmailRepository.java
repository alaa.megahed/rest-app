package com.sumerge.program.database.repository;

import com.sumerge.program.database.entity.Email;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class EmailRepository {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepository.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Email> getAllEmailes() {
        LOGGER.info("fetching all Emailes");
        try {
            return em.createNamedQuery("Email.findAll", Email.class).getResultList();
        }
        catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public Email getEmailById(String id) {
        try {
            return em.find(Email.class, id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }


    public void addEmail(Email email) {
        LOGGER.info("Saving new Email " + email);
        try {
            em.persist(email);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void removeEmail(String id) {
        LOGGER.info("Removing an Email " + id);
        try {
            Email email = em.find(Email.class, id);
            em.remove(email);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateEmail(Email email) {
        LOGGER.info("Updating an Email " + email);
        try {
            em.merge(email);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }
}