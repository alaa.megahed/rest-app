package com.sumerge.program.database.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "DEPARTMENT")
@NamedQueries(
        @NamedQuery(name = "Department.findAll", query = "SELECT d FROM Department d")
)
public class Department implements Serializable {

    @Id
    @Column(name = "DEPTCODE", length = 4)
    private String deptcode;

    @Column(name = "DEPTNAME", length = 60)
    private String deptname;

    @JsonIgnore
    @JoinColumn(name = "MANAGER")
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee manager;

    public String getDeptcode() {
        return deptcode;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }


    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }
}
