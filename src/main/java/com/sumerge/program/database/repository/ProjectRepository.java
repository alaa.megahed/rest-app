package com.sumerge.program.database.repository;

import com.sumerge.program.database.entity.Project;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class ProjectRepository {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepository.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Project> getAllProjectes() {
        LOGGER.info("fetching all Projectes");
        try {
            return em.createNamedQuery("Project.findAll", Project.class).getResultList();
        }
        catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public Project getProjectById(String id) {
        try {
            return em.find(Project.class, id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }


    public void addProject(Project project) {
        LOGGER.info("Saving new Project " + project);
        try {
            em.persist(project);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void removeProject(String id) {
        LOGGER.info("Removing an Project " + id);
        try {
            Project project = em.find(Project.class, id);
            em.remove(project);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateProject(Project project) {
        LOGGER.info("Updating an Project " + project);
        try {
            em.merge(project);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }
}