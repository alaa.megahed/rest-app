package com.sumerge.program.database.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "EMPLOYEE")
@NamedQueries({
        @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e"),
        @NamedQuery(name = "Employee.findByName", query = "SELECT e FROM Employee e WHERE e.givenName = :name")
})
@XmlRootElement
@EntityListeners(EmployeeListener.class)
public class Employee implements Serializable {

    @Id
    @Column(name = "EMPID", length = 6, unique = true)
    private String empId;

    @JoinColumn(name = "DEPTCODE")
    @ManyToOne
    private Department department;

    @Column(name = "JOBTITLE", length = 30)
    private String jobTitle;

    @Column(name = "GIVENNAME", length = 30)
    private String givenName;

    @Column(name = "FAMILYNAME", length = 30)
    private String familyName;

    @Column(name = "COMMONNAME", length = 60)
    private String commonName;

    @Column(name = "NAMETITLE", length = 6)
    private String nameTitle;

    @JsonIgnore
    @ManyToMany (fetch = FetchType.LAZY)
    @JoinTable(
            name = "PROJECTMEMBER",
            joinColumns = @JoinColumn(name = "EMPID"),
            inverseJoinColumns = @JoinColumn(name = "PROJID"))
    List<Project> projects;



    @Transient
    private int idInt;

    private int formatId(String empId) {
        return Integer.parseInt(empId.substring(1));
    }

    public int getIdInt() {
        return formatId(empId);
    }

    public void setIdInt(int idInt) {
        this.idInt = idInt;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getNameTitle() {
        return nameTitle;
    }

    public void setNameTitle(String nameTitle) {
        this.nameTitle = nameTitle;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}
