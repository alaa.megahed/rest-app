package com.sumerge.program.database.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ADDRESS")
@NamedQueries(
        @NamedQuery(name = "Address.findAll", query = "SELECT a FROM Address a")
)
public class Address implements Serializable {

    @Id
    @Column(name = "ADDRESSID")
    private int addressId;


    @ManyToOne
    @JoinColumn(name = "EMPID")
    private Employee employee;

    @Column(name = "ADDLINE1", length = 50)
    private String addLine1;

    @Column(name = "ADDLINE2", length = 50)
    private String addLine2;

    @Column(name = "CITY", length = 20)
    private String city;

    @Column(name = "REGION", length = 20)
    private String region;

    @Column(name = "COUNTRY", length = 20)
    private String country;

    @Column(name = "POSTCODE", length = 10)
    private String postcode;

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getAddLine1() {
        return addLine1;
    }

    public void setAddLine1(String addLine1) {
        this.addLine1 = addLine1;
    }

    public String getAddLine2() {
        return addLine2;
    }

    public void setAddLine2(String addLine2) {
        this.addLine2 = addLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }


}
