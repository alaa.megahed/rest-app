package com.sumerge.program.rest;

import com.sumerge.program.database.entity.Department;
import com.sumerge.program.database.repository.DepartmentRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("employee")
public class DepartmentResources {

    private static final Logger LOGGER = Logger.getLogger(EmployeeResources.class.getName());

    @Context
    private SecurityContext securityContext;

    @EJB
    private DepartmentRepository repo;

    @GET
    @Path("all")
    public Response getAllDepartmentes() {
        try {
            return Response.ok().entity(repo.getAllDepartmentes()).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @GET
    @Path("{id}")
    public Response getDepartmentById(@PathParam("id") String id) {
        try {
            return Response.ok().entity(repo.getDepartmentById(id)).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @POST
    public Response post(Department department) {
        try {
            repo.addDepartment(department);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response removeDepartmentById(@PathParam("id") String id) {
        try {
            repo.removeDepartment(id);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @PUT
    public Response updateEmployee(Department department){
        try {
            repo.updateDepartment(department);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }
}
