package com.sumerge.program.rest;

import com.sumerge.program.database.entity.Phonenumber;
import com.sumerge.program.database.repository.PhonenumberRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("employee")
public class PhonenumberResources {

    private static final Logger LOGGER = Logger.getLogger(EmployeeResources.class.getName());

    @Context
    private SecurityContext securityContext;

    @EJB
    private PhonenumberRepository repo;

    @GET
    @Path("all")
    public Response getAllPhonenumberes() {
        try {
            return Response.ok().entity(repo.getAllPhonenumberes()).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @GET
    @Path("{id}")
    public Response getPhonenumberById(@PathParam("id") String id) {
        try {
            return Response.ok().entity(repo.getPhonenumberById(id)).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @POST
    public Response post(Phonenumber phonenumber) {
        try {
            repo.addPhonenumber(phonenumber);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response removePhonenumberById(@PathParam("id") String id) {
        try {
            repo.removePhonenumber(id);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @PUT
    public Response updateEmployee(Phonenumber phonenumber){
        try {
            repo.updatePhonenumber(phonenumber);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }
}
