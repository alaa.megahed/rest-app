package com.sumerge.program.rest;

import com.sumerge.program.database.entity.Email;
import com.sumerge.program.database.repository.EmailRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("employee")
public class EmailResources {

    private static final Logger LOGGER = Logger.getLogger(EmployeeResources.class.getName());

    @Context
    private SecurityContext securityContext;

    @EJB
    private EmailRepository repo;

    @GET
    @Path("all")
    public Response getAllEmailes() {
        try {
            return Response.ok().entity(repo.getAllEmailes()).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @GET
    @Path("{id}")
    public Response getEmailById(@PathParam("id") String id) {
        try {
            return Response.ok().entity(repo.getEmailById(id)).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @POST
    public Response post(Email email) {
        try {
            repo.addEmail(email);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response removeEmailById(@PathParam("id") String id) {
        try {
            repo.removeEmail(id);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @PUT
    public Response updateEmployee(Email email){
        try {
            repo.updateEmail(email);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }
}
