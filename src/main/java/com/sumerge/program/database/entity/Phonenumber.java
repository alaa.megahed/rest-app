package com.sumerge.program.database.entity;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PHONENUMBER")
@NamedQueries(
        @NamedQuery(name = "Phonenumber.findAll", query = "SELECT p FROM Phonenumber p")
)
public class Phonenumber implements Serializable {

    @Id
    @Column(name = "PHONEID")
    private int phoneId;


    @ManyToOne
    @JoinColumn(name = "EMPID")
    private Employee employee;

    @Column(name = "LOCALNUM", length = 15)
    private String localNum;

    public int getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(int phoneId) {
        this.phoneId = phoneId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getLocalNum() {
        return localNum;
    }

    public void setLocalNum(String localNum) {
        this.localNum = localNum;
    }

    public String getIntlPreix() {
        return intlPreix;
    }

    public void setIntlPreix(String intlPreix) {
        this.intlPreix = intlPreix;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    @Column(name = "INTLPREFIX", length = 5)
    private String intlPreix;

    @Column(name = "PHONETYPE", length = 11)
    private String phoneType;

}
