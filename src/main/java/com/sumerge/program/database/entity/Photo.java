package com.sumerge.program.database.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PHOTO")
@NamedQueries(
        @NamedQuery(name = "Photo.findAll", query = "SELECT p FROM Photo p")
)
public class Photo implements Serializable {

    @Id
    @Column(name = "PHOTOID")
    private int photoId;


    @ManyToOne
    @JoinColumn(name = "EMPID")
    private Employee employee;

    @Column(name = "IMAGENAME")
    private String imageName;

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
