package com.sumerge.program.database.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "PROJECT")
@NamedQueries(
        @NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p")
)
public class Project implements Serializable {

    @Id
    @Column(name = "PROJID", length = 6)
    private String projId;

    @Column(name = "PROJNAME", length = 80)
    private String projectName;

    @Column(name = "STARTDATE")
    private Date startDate;

    @Column(name = "TARGETDATE")
    private Date targetDate;

    @Column(name = "STATUS", length = 7)
    private String status;

    @JsonIgnore
    @ManyToMany (fetch = FetchType.LAZY)
    @JoinTable(
            name = "PROJECTMEMBER",
            joinColumns = @JoinColumn(name = "PROJID"),
            inverseJoinColumns = @JoinColumn(name = "EMPID"))
    private List<Employee> employees;


    public String getProjId() {
        return projId;
    }

    public void setProjId(String projId) {
        this.projId = projId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }


}
