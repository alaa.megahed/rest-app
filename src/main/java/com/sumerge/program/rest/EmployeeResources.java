package com.sumerge.program.rest;


import com.sumerge.program.database.entity.Employee;
import com.sumerge.program.database.repository.EmployeeRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("employee")
public class EmployeeResources {

    private static final Logger LOGGER = Logger.getLogger(EmployeeResources.class.getName());

    @Context
    private SecurityContext securityContext;

    @EJB
    private EmployeeRepository repo;

    @GET
    @Path("all")
    public Response getAllEmployees() {
        try {
            return Response.ok().entity(repo.getAllEmployees()).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @GET
    @Path("{id}")
    public Response getEmployeeById(@PathParam("id") String id) {
        try {
            return Response.ok().entity(repo.getEmployeeById(id)).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @GET
    @Path("name/{name}")
    public Response getEmployeeByName(@PathParam("name") String name) {
        try {
            return Response.ok().entity(repo.getEmployeeByName(name)).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @GET
    @Path("{id}/projects")
    public Response getEmployeeProjects(@PathParam("id") String id) {
        try {
            return Response.ok().entity(repo.getEmployeeWithProjects(id)).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @POST
    public Response post(Employee employee) {
        try {
            repo.addEmployee(employee);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response removeEmployeeById(@PathParam("id") String id) {
        try {
            repo.removeEmployee(id);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @PUT
    public Response updateEmployee(Employee employee){
        try {
            repo.updateEmployee(employee);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }
}
