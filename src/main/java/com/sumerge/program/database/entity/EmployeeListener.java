package com.sumerge.program.database.entity;

import com.sumerge.program.database.repository.EmployeeRepository;

import javax.persistence.PrePersist;
import java.util.logging.Logger;


public class EmployeeListener {
    private static final Logger LOGGER = Logger.getLogger(EmployeeRepository.class.getName());

    @PrePersist
    public void printStatus(Employee employee) {
        LOGGER.info("In EmployerListener .. before adding employee");
    }
}
