package com.sumerge.program.rest;

import com.sumerge.program.database.entity.Address;
import com.sumerge.program.database.repository.AddressRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("employee")
public class AddressResources {

    private static final Logger LOGGER = Logger.getLogger(EmployeeResources.class.getName());

    @Context
    private SecurityContext securityContext;

    @EJB
    private AddressRepository repo;

    @GET
    @Path("all")
    public Response getAllAddresses() {
        try {
            return Response.ok().entity(repo.getAllAddresses()).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @GET
    @Path("{id}")
    public Response getAddressById(@PathParam("id") String id) {
        try {
            return Response.ok().entity(repo.getAddressById(id)).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @POST
    public Response post(Address address) {
        try {
            repo.addAddress(address);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response removeAddressById(@PathParam("id") String id) {
        try {
            repo.removeAddress(id);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @PUT
    public Response updateEmployee(Address address){
        try {
            repo.updateAddress(address);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }
}
