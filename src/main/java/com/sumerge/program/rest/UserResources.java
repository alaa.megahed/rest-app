package com.sumerge.program.rest;

import com.sumerge.program.database.entity.User;
import com.sumerge.program.database.repository.UserRepository;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author Ahmed Anwar
 */
@Path("/")
@RequestScoped
public class UserResources
{
    private static final Logger LOGGER = Logger.getLogger(UserResources.class.getName());

    @Context
    private SecurityContext securityContext;

    @EJB
    private UserRepository repo;

    @GET
	@Path("all")
    @Produces(APPLICATION_JSON)
    public Response get() {
		LOGGER.info("Entering get with user " + securityContext.getUserPrincipal().toString());
		try {
			return Response.ok().
					entity(repo.getAllUsers()).
					build();
		} catch (Exception e) {
			return Response.serverError().
					entity(e).
					build();
		}
    }

	@GET
	@PathParam("id")
	public Response getEmployeeById(@PathParam("id") int id) {
		try {
			return Response.ok().entity(repo.getUserById(id)).build();
		} catch (Exception e) {
			return Response.serverError().entity(e).build();
		}
	}

    @POST
    @Consumes(APPLICATION_JSON)
    public Response post(User user) {
        LOGGER.info("Entering post with user " + securityContext.getUserPrincipal().toString());
		try {
			repo.addUser(user);
			return Response.ok().
					build();
		} catch (Exception e) {
			return Response.serverError().
					entity(e.getMessage()).
					build();
		}
    }
}
