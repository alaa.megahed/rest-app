package com.sumerge.program.rest;

import com.sumerge.program.database.entity.Project;
import com.sumerge.program.database.repository.ProjectRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("employee")
public class ProjectResources {

    private static final Logger LOGGER = Logger.getLogger(EmployeeResources.class.getName());

    @Context
    private SecurityContext securityContext;

    @EJB
    private ProjectRepository repo;

    @GET
    @Path("all")
    public Response getAllProjectes() {
        try {
            return Response.ok().entity(repo.getAllProjectes()).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @GET
    @Path("{id}")
    public Response getProjectById(@PathParam("id") String id) {
        try {
            return Response.ok().entity(repo.getProjectById(id)).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @POST
    public Response post(Project project) {
        try {
            repo.addProject(project);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response removeProjectById(@PathParam("id") String id) {
        try {
            repo.removeProject(id);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @PUT
    public Response updateEmployee(Project project){
        try {
            repo.updateProject(project);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }
}
