package com.sumerge.program.database.repository;

import com.sumerge.program.database.entity.Employee;
import com.sumerge.program.database.entity.Project;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class EmployeeRepository {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepository.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Employee> getAllEmployees() {
        LOGGER.info("fetching all employees");
        try {
            return em.createNamedQuery("Employee.findAll", Employee.class).getResultList();
        }
        catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public Employee getEmployeeById(String empId) {
        try {
            Employee employee = em.find(Employee.class, empId);
//            em.detach(employee);
            return employee;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public List<Project> getEmployeeWithProjects(String empId) {
        try {
            Employee employee = em.find(Employee.class, empId);
            return employee.getProjects();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }


    public Employee getEmployeeByName(String name) {
        LOGGER.info("fetching all employees");
        try {
            return em.createNamedQuery("Employee.findByName", Employee.class).setParameter("name", name).getSingleResult();
        }
        catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void addEmployee(Employee employee) {
        LOGGER.info("Saving new employee " + employee);
        try {
            em.persist(employee);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void removeEmployee(String empId) {
        LOGGER.info("Removing an employee " + empId);
        try {
            Employee employee = em.find(Employee.class, empId);
            em.remove(employee);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateEmployee(Employee employee) {
        LOGGER.info("Updating an employee " + employee.getEmpId());
        try {
            em.merge(employee);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }
}