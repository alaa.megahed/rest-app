package com.sumerge.program.database.repository;

import com.sumerge.program.database.entity.Address;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class AddressRepository {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepository.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Address> getAllAddresses() {
        LOGGER.info("fetching all addresses");
        try {
            return em.createNamedQuery("Address.findAll", Address.class).getResultList();
        }
        catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public Address getAddressById(String id) {
        try {
            return em.find(Address.class, id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }


    public void addAddress(Address address) {
        LOGGER.info("Saving new address " + address);
        try {
            em.persist(address);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void removeAddress(String id) {
        LOGGER.info("Removing an address " + id);
        try {
            Address address = em.find(Address.class, id);
            em.remove(address);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateAddress(Address address) {
        LOGGER.info("Updating an address " + address);
        try {
            em.merge(address);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }
}