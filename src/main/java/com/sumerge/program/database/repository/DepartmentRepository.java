package com.sumerge.program.database.repository;

import com.sumerge.program.database.entity.Department;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class DepartmentRepository {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepository.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Department> getAllDepartmentes() {
        LOGGER.info("fetching all Departmentes");
        try {
            return em.createNamedQuery("Department.findAll", Department.class).getResultList();
        }
        catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public Department getDepartmentById(String id) {
        try {
            return em.find(Department.class, id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }


    public void addDepartment(Department department) {
        LOGGER.info("Saving new Department " + department);
        try {
            em.persist(department);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void removeDepartment(String id) {
        LOGGER.info("Removing an Department " + id);
        try {
            Department department = em.find(Department.class, id);
            em.remove(department);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateDepartment(Department department) {
        LOGGER.info("Updating an Department " + department);
        try {
            em.merge(department);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }
}