package com.sumerge.program.database.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "EMAIL")
@NamedQueries(
        @NamedQuery(name = "Email.findAll", query = "SELECT e FROM Email e")
)
public class Email implements Serializable {

    @Id
    @Column(name = "EMAILID")
    private int emailId;

    public int getEmailId() {
        return emailId;
    }

    public void setEmailId(int emailId) {
        this.emailId = emailId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailType() {
        return emailType;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    @ManyToOne
    @JoinColumn(name = "EMPID")
    private Employee employee;

    @Column(name = "EMAILADDRESS", length = 40)
    private String emailAddress;

    @Column(name = "EMAILTYPE", length = 5)
    private String emailType;
}
