package com.sumerge.program.database.repository;

import com.sumerge.program.database.entity.Photo;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class PhotoRepository {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepository.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Photo> getAllPhotoes() {
        LOGGER.info("fetching all Photoes");
        try {
            return em.createNamedQuery("Photo.findAll", Photo.class).getResultList();
        }
        catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public Photo getPhotoById(String id) {
        try {
            return em.find(Photo.class, id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }


    public void addPhoto(Photo photo) {
        LOGGER.info("Saving new Photo " + photo);
        try {
            em.persist(photo);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void removePhoto(String id) {
        LOGGER.info("Removing an Photo " + id);
        try {
            Photo photo = em.find(Photo.class, id);
            em.remove(photo);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updatePhoto(Photo photo) {
        LOGGER.info("Updating an Photo " + photo);
        try {
            em.merge(photo);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }
}