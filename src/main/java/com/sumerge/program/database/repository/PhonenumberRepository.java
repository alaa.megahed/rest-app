package com.sumerge.program.database.repository;

import com.sumerge.program.database.entity.Phonenumber;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class PhonenumberRepository {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepository.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Phonenumber> getAllPhonenumberes() {
        LOGGER.info("fetching all Phonenumberes");
        try {
            return em.createNamedQuery("Phonenumber.findAll", Phonenumber.class).getResultList();
        }
        catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public Phonenumber getPhonenumberById(String id) {
        try {
            return em.find(Phonenumber.class, id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }


    public void addPhonenumber(Phonenumber phonenumber) {
        LOGGER.info("Saving new Phonenumber " + phonenumber);
        try {
            em.persist(phonenumber);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void removePhonenumber(String id) {
        LOGGER.info("Removing an Phonenumber " + id);
        try {
            Phonenumber phonenumber = em.find(Phonenumber.class, id);
            em.remove(phonenumber);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updatePhonenumber(Phonenumber phonenumber) {
        LOGGER.info("Updating an Phonenumber " + phonenumber);
        try {
            em.merge(phonenumber);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }
}