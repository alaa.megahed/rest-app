package com.sumerge.program.rest;

import com.sumerge.program.database.entity.Photo;
import com.sumerge.program.database.repository.PhotoRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("employee")
public class PhotoResources {

    private static final Logger LOGGER = Logger.getLogger(EmployeeResources.class.getName());

    @Context
    private SecurityContext securityContext;

    @EJB
    private PhotoRepository repo;

    @GET
    @Path("all")
    public Response getAllPhotoes() {
        try {
            return Response.ok().entity(repo.getAllPhotoes()).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @GET
    @Path("{id}")
    public Response getPhotoById(@PathParam("id") String id) {
        try {
            return Response.ok().entity(repo.getPhotoById(id)).build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @POST
    public Response post(Photo photo) {
        try {
            repo.addPhoto(photo);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response removePhotoById(@PathParam("id") String id) {
        try {
            repo.removePhoto(id);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }

    @PUT
    public Response updateEmployee(Photo photo){
        try {
            repo.updatePhoto(photo);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
    }
}
